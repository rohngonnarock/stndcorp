﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;

namespace StndCorp.Models
{
    public class Gallery
    {
        public string strname { get; set; }
        public string[] strfile { get; set; }
        public IList<string> strlistmain { get; set; }

        public void getfiles()
        {
            string str = HttpContext.Current.Server.MapPath("..");
            // var file = Directory.GetFiles(str + "\\content\\images\\gallery\\").ToList();
            IEnumerable<string> file1 = Directory.EnumerateFiles(str + "\\content\\images\\gallery\\");
            List<string> strlist = new List<string>();
            DirectoryInfo d = new DirectoryInfo(str + "\\content\\images\\gallery\\"); //Assuming Test is your Folder
            FileInfo[] Files = d.GetFiles(); //Getting Text files

            foreach (FileInfo file in Files)
            {
                try
                {
                    str = file.Name;
                    
                    strlist.Add(str);
                }
                catch (Exception)
                {
                }


            }
            strlistmain = strlist;
            
        }
    }
}