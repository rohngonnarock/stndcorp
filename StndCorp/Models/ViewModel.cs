﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
namespace StndCorp.Models
{
    public class ViewModel
    {
        public Gallery gal { get; set; }
        [Key]
        public Customer cust { get; set; }
    }
}