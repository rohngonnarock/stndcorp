﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using StndCorp.Models;
using System.IO;
using StndCorp.Models.modelbinder;
using StndCorp.DAL;
using System.Data;

namespace StndCorp.Controllers
{
    public class HomeController : Controller
    {
        [OutputCache(Duration = 7200, VaryByParam = "none")]
        public ActionResult Index()
        {
            return View();
        }

        [OutputCache(Duration = 7200, VaryByParam = "none")]
        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        [OutputCache(Duration = 7200, VaryByParam = "none")]
        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        [OutputCache(Duration = 7200, VaryByParam = "none")]
        public ActionResult slider()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        [OutputCache(Duration = 7200, VaryByParam = "none")]
        public ActionResult Gallery()
        {
            Customer objcus = new Customer();
            objcus.CustomerCode = "InV1001";
            objcus.CustomerName = "Rahul Prajapati";

            DbConnStr Dal = new DbConnStr();
            Dal.Customers.Add(objcus);
            Dal.SaveChanges();

            string str = Server.MapPath("..");
            string[] strfile = Directory.GetFiles(str + "/content/images/gallery/");
           // Gallery gal = new Models.Gallery();
            
            ViewBag.Message = strfile.ToString();

            return View();
        }

        [OutputCache(Duration = 7200, VaryByParam = "none")]
        public ActionResult Gal()
        {
            Customer objcus = new Customer();
            DbConnStr dal = new DbConnStr();

            List<Customer> cusroll = (from x in dal.Customers
                                      where x.CustomerName == "Rahul Prajapat"
                                      select x).ToList<Customer>(); 
            objcus.customers = cusroll;


            return View("Gallery");
        }

        [OutputCache(Duration = 7200, VaryByParam = "none")]
        public ActionResult Gallery1([ModelBinder(typeof(ModelBinder ))] Gallery gal) 
        {
            
            gal.getfiles();
            string[] str = gal.strfile;
                        return View(gal);
        }
    }
}