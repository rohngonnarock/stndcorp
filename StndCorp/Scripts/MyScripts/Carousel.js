﻿angular.module('ui.bootstrap.demo', ['ui.bootstrap']);
angular.module('ui.bootstrap.demo').controller('CarouselDemoCtrl', function ($scope) {
    $scope.myInterval = 5000;
    var slides = $scope.slides = [];
    for (var i = 1; i < 4; i++) {
        $scope.slides.push({ text: 'cats!', image: '/Content/images/slider/slider' + i + '.jpg' });
    }
});