﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(StndCorp.Startup))]
namespace StndCorp
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
